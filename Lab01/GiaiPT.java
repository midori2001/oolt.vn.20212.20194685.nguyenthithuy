package Lab01;

import static java.lang.Math.sqrt;
import javax.swing.JOptionPane;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author ADMIN
 */
public class GiaiPT {
    public static void main(String[] args) {
        float a, b, c, delta, menu;
        String smenu, sa, sb, sc, kq;
        float a11, a12, a21, a22, b1, b2;
        String s11, s12, s21, s22, sb1, sb2;
        do {
           smenu = JOptionPane.showInputDialog(null, "  \t ---------MENU---------\n1. Giải phương trình bậc nhất ax + b = 0\n2. Giải hệ phương trình bậc nhất 2 ẩn\n3. Giải phương trình bậc hai ax^2 + bx + c = 0\n *Phím khác: Thoát\n\n", "MENU", JOptionPane.INFORMATION_MESSAGE);
           menu = Float.parseFloat(smenu);
           if (menu == 1){
            sa = JOptionPane.showInputDialog(null, "Mời bạn nhập số a: ", "Giải phương trình ax + b = 0", JOptionPane.INFORMATION_MESSAGE);
            a = Float.parseFloat(sa);
            sb = JOptionPane.showInputDialog(null, "Mời bạn nhập số b: ", "Giải phương trình ax + b = 0", JOptionPane.INFORMATION_MESSAGE);
            b = Float.parseFloat(sb);
            if (a == 0 && b != 0) {
                kq = "Phương trình vô nghiệm";
            } else if (a == 0 && b == 0) {
                kq = "Phương trình có vô số nghiệm";
            } else {
                kq = "Phương trình có nghiệm duy nhất: \nx = -(" + b + ")/" + a + " = " + (-b / a);
            }
        JOptionPane.showMessageDialog(null, kq);
        } else if (menu == 2){
            s11 = JOptionPane.showInputDialog(null, "a11x1 + a12x2 = b1\na21x1 + a22x2 = b2\nMời bạn nhập hệ số a11: ", "Giải hệ phương trình bậc nhất 2 ẩn", JOptionPane.INFORMATION_MESSAGE);
            a11 = Float.parseFloat(s11);
            s12 = JOptionPane.showInputDialog(null, "a11x1 + a12x2 = b1\na21x1 + a22x2 = b2\nMời bạn nhập hệ số a12: ", "Giải hệ phương trình bậc nhất 2 ẩn", JOptionPane.INFORMATION_MESSAGE);
            a12 = Float.parseFloat(s12);
            sb1 = JOptionPane.showInputDialog(null, "a11x1 + a12x2 = b1\na21x1 + a22x2 = b2\nMời bạn nhập hệ số b1: ", "Giải hệ phương trình bậc nhất 2 ẩn", JOptionPane.INFORMATION_MESSAGE);
            b1 = Float.parseFloat(sb1);
            s21 = JOptionPane.showInputDialog(null, "a11x1 + a12x2 = b1\na21x1 + a22x2 = b2\nMời bạn nhập hệ số a21: ", "Giải hệ phương trình bậc nhất 2 ẩn", JOptionPane.INFORMATION_MESSAGE);
            a21 = Float.parseFloat(s21);
            s22 = JOptionPane.showInputDialog(null, "a11x1 + a12x2 = b1\na21x1 + a22x2 = b2\nMời bạn nhập hệ số a22: ", "Giải hệ phương trình bậc nhất 2 ẩn", JOptionPane.INFORMATION_MESSAGE);
            a22 = Float.parseFloat(s22);
            sb2 = JOptionPane.showInputDialog(null, "a11x1 + a12x2 = b1\na21x1 + a22x2 = b2\nMời bạn nhập hệ số b2: ", "Giải hệ phương trình bậc nhất 2 ẩn", JOptionPane.INFORMATION_MESSAGE);
            b2 = Float.parseFloat(sb2);
            a = a11*a22 - a21*a12; //D
            b = b1*a22 - b2*a12;  //D1
            c = a11*b2 - a21*b1;  //D2
            if(a != 0) {
                kq = "Hệ phương trình có nghiệm duy nhất \n(x1, x2) = ("+b/a+", "+c/a+")";
            } else {
                if (b == 0 && c == 0){
                    kq = "Hệ phương trình có vô số nghiệm";
                } else {
                    kq = "Hệ phương trình vô nghiệm";
                }
            }
            JOptionPane.showMessageDialog(null, kq);
        } else if (menu == 3){
            sa = JOptionPane.showInputDialog(null, "Mời bạn nhập số a: ", "Giải phương trình ax^2 + bx + c = 0", JOptionPane.INFORMATION_MESSAGE);
            a = Float.parseFloat(sa);
            sb = JOptionPane.showInputDialog(null, "Mời bạn nhập số b: ", "Giải phương trình ax^2 + bx + c = 0", JOptionPane.INFORMATION_MESSAGE);
            b = Float.parseFloat(sb);
            sc = JOptionPane.showInputDialog(null, "Mời bạn nhập số c: ", "Giải phương trình ax^2 + bx + c = 0", JOptionPane.INFORMATION_MESSAGE);
            c = Float.parseFloat(sc);
            if (a == 0) {
                if (b == 0 && c != 0) {
                    kq = "Phương trình vô nghiệm";
                } else if (b == 0 && c == 0) {
                    kq = "Phương trình có vô số nghiệm";
                } else {
                    kq = "Phương trình có nghiệm duy nhất: \nx = " + (-c / b);
                }
            } else {
                delta = b * b - 4 * a * c;
                if (delta == 0) {
                    kq = "Phương trình có nghiệm thực duy nhất: \nx = " + (-b / (2 * a));
                } else if (delta > 0) {
                    kq = "Phương trình có hai nghiệm thực phân biệt: \nx1 = " + ((-b + sqrt(delta)) / (2 * a)) + "\nx2 = " + ((-b - sqrt(delta)) / (2 * a));
                } else {
                   kq = "Phương trình không có nghiệm thực";
                }
            }
            JOptionPane.showMessageDialog(null, kq);
        } 
        } while (smenu.compareTo("1") == 0 || smenu.compareTo("2") == 0 || smenu.compareTo("3") == 0); 
    }
}
