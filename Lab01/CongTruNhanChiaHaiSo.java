/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Lab01;

import javax.swing.JOptionPane;

/**
 *
 * @author ADMIN
 */
public class CongTruNhanChiaHaiSo {
    public static void main(String[] args) {
        String strNum1, strNum2;
        double x1, x2, thuong;
        String strNotification = "You're just entered: ";
        String chiaLoi ="";
        String chiaDuoc = "";

        strNum1 = JOptionPane.showInputDialog(null, "Plese input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
        strNotification += strNum1 + " and ";
        x1 = Double.parseDouble(strNum1);
        
        strNum2 = JOptionPane.showInputDialog(null, "Plese input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
        strNotification += strNum2;
        x2 = Double.parseDouble(strNum2);
        if(x2 == 0){
            chiaDuoc = "Ko thuc hien duoc phep chia";
        }else {
            thuong = x1/x2;
            chiaDuoc = Double.toString(thuong);
        }
        JOptionPane.showMessageDialog(null, "Tong: " + (x1+x2) + "\nHieu: "+ (x1-x2) + "\nTich: "+ (x1*x2) + "\nThuong: " + chiaDuoc, "Show two numbers", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}
