/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab03.homework8;

/**
 *
 * @author ADMIN
 */
public class DataTest {
    public static void main(String[] args) {
        System.out.println("Date a: ");
        MyDate a = new MyDate("29th", "Se", 2001);
        a.print();
        System.out.println("\nDate b: ");
        MyDate b = new MyDate();
        b.print();
        b.setDay("22");
        b.print();
        System.out.println("\nDate c:");
        MyDate c = new MyDate("29", "Feb", 1600);
        c.print();
        System.out.println("\nDate d:");
        MyDate d = new MyDate("Sept. 28sn 2001");
        d.print();
        d.accept();
        d.print();
        
    }
}
