/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab03.homework8;

import java.time.LocalDate;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 *
 * @author ADMIN
 */
public class MyDate {
    private String day;
    private String month;
    private int year;
    
    
    public String getDay() {
        return day;
    }
    
     public int totalDayInMonth(String month){
        int i = 31;
//        System.out.println("Thang nay la: " + month);
        month = month.toLowerCase();
        switch (month){
            case "april":
            case "apr.":
            case "apr":
            case "4":
            case "june":
            case "jun":
            case "6":
            case "september":
            case "sept.":
            case "sep":
            case "9":
            case "november":
            case "nov.":
            case "nov":
            case "11":
                i = 30;
                break;
            case "february":
            case "feb.":
            case "feb":
            case "2":
                if((year % 400 == 0) || (year % 100 != 0 && year % 4 == 0)){
                    i = 29;
                } else i = 28;
                break;
        }
        return i;
    }
//    public boolean IntDay(String x){
//        if (Pattern.matches("[0-9]+", x)) {
//               return true;
//            } else return false;
//    }
    String abbDay[] = {"1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th", "10th", "11th", "12th", "13th", "14th", "15th", "16th", "17th"
                        , "18th", "19th", "20th", "21st", "22nd", "23rd", "24th", "25th", "26th", "27th", "28th", "29th", "30th", "31st", "1", "2",
                        "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", 
                        "25", "26", "27", "28", "29", "30", "31"};
    private String checkDay(String day){
        day = day.trim();
        String kq = "";
        int dayNo = -1;
        int mark = -1;
               for (int i = 0; i < abbDay.length; i++) {
               if(day.compareToIgnoreCase(abbDay[i]) == 0){
                   mark = i;
                   break;
               }
            }
               if(mark < 31) dayNo = mark + 1;
               else dayNo = mark - 30;
        if(dayNo > 0 && dayNo <= totalDayInMonth(month)){
            kq = abbDay[dayNo - 1];
        } else {
            kq = "Invalid-day";
        }
        //System.out.println("***: "+ kq);
        return kq;
    }
    
    public void setDay(String day) {
         this.day = checkDay(day);
    }

    public String getMonth() {
        return month;
    }

    String nameMonth [] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December",
        "Jan.", "Feb.", "Mar.", "Apr.", "Aug.", "Sept.", "Oct.", "Nov.", "Dec.",
        "Jan", "Feb", "Mar", "Apr", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private String checkMonth(String month){
        month = month.trim();
        for (int i = 0; i < nameMonth.length; i++) {
            if(month.compareToIgnoreCase(nameMonth[i]) == 0) return month;
        }
        return "Invalid-month";
    }
    public void setMonth(String month) {
        this.month = checkMonth(month);
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if(year >= 0) 
            this.year = year;
        else   
            System.out.println("Invalid-year");
    }
    
    private String[] currentDate(){
        LocalDate date = java.time.LocalDate.now(); 
        //System.out.println(date);
        return date.toString().split("-");
    }
    
    public MyDate() {
        super();
        String arr[] = currentDate();
        this.year = Integer.parseInt(arr[0]);
        this.month = nameMonth[Integer.parseInt(arr[1])-1];
        int a = Integer.parseInt(arr[2]);
        this.day = checkDay(Integer.toString(a));
        
    }
    
    public MyDate(String day, String month, int year) {
        super();
        this.month = checkMonth(month);
        if(year >= 0) 
            this.year = year;
        else {
            this.year = -999999;
            System.out.println("Invalid-year");
        }        
        this.day = checkDay(day);
    }
    
    public MyDate(String s){
        s = s.trim();
        String arr[] = s.split(" ");
        this.year = Integer.parseInt(arr[2]);
        this.month = checkMonth(arr[0]);
        this.day = checkDay(arr[1]);
        
    }
    
    
//    private String getDayByNumber(String day){
//        String abb = "";
//        switch(day){
//            case "1", "21", "31" -> abb = "st";
//            case "2", "22" -> abb = "nd";
//            case "3", "23" -> abb = "rd";
//            case "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "24", "25", "26", "27", "28", "29", "30" -> abb = "th";
//            default -> {
//            }
//        }
//        return day + abb;
//    }
    
    public void accept(){
        System.out.print("Enter date (Ex: April 2 2022, Apr 2nd 2022): ");
        Scanner sc = new Scanner(System.in);
        String date = sc.nextLine();
        String arr[] = date.split(" ");
        this.year = Integer.parseInt(arr[2]);
        this.month = checkMonth(arr[0]);
        this.day = checkDay(arr[1]);
    }
    
    public void print(){
//        String arr[] = currentDate();
//        System.out.println(nameMonth[Integer.parseInt(arr[1])-1] +" "+  getDayByNumber(arr[2]) +" "+ arr[0]);
        System.out.println(this.month + " " + this.day + " " + this.year);
    }
    
    

    
    
    
}
