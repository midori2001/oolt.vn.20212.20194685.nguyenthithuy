/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hust.soict.gui.swing;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.JTextField;

/**
 *
 * @author ADMIN
 */
public class SwingCounter extends JFrame {
    private JTextField tfCount; // Use Swing's JTextField instead of AWT's TextField
    private JButton btnCount; // Using Swing's JButton instead of AWT's Button
    private int count = 0;
    
    public SwingCounter(){
        Container cp = getContentPane();
        cp.setLayout(new FlowLayout());
        cp.add(new JLabel("Counter"));
        
        tfCount = new JTextField("0");
        tfCount.setColumns(5);
        tfCount.setEditable(true);
        cp.add(tfCount);
        
        btnCount = new JButton("Count");
        cp.add(btnCount);
        btnCount.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ++count;
                tfCount.setText(count + "");
            }
        });
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Swing Counter");
        setSize(400, 150);
        setVisible(true);
    }
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new SwingCounter();
            }
        });
        

    }
}
