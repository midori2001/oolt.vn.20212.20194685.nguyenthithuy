/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab02;

import java.util.Scanner;

/**
 *
 * @author ADMIN
 */
public class HW7 {
    public static void main(String[] args) {
        // cộng hai ma trận cùng kích cỡ
        Scanner sc = new Scanner(System.in);
        double A[][] = {
                        {2, 5, -6, 9},
                        {9, 15, 200.2, 12},
                        {1, 2.5, 6, -1}
                    };
        double B[][] = {
                        {9.28, 0.2, -3, 6},
                        {4, 5, 1, 100},
                        {7.167, -5, 10, 28.9}
        };
        double C[][] = new double [3][4];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                C[i][j] = A[i][j] + B[i][j];
            }
        }
        System.out.println("Ma tran A: ");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print( A[i][j] +"\t");
            }
            System.out.println("");
        }
        System.out.println("\n\nMa tran B: ");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print( B[i][j] +"\t");
            }
            System.out.println("");
        }
        System.out.println("\n\nTong cua hai ma tran A + B");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print( C[i][j] +"\t");
            }
            System.out.println("");
        }
    }
}
