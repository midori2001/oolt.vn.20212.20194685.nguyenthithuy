/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab02;

import java.util.Scanner;

/**
 *
 * @author ADMIN
 */
public class HW5 {
    public static boolean thangHopLe(String month){
        String name [] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December",
        "Jan.", "Feb.", "Mar.", "Apr.", "Aug.", "Sept.", "Oct.", "Nov.", "Dec.",
        "Jan", "Feb", "Mar", "Apr", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",};
        for (int i = 0; i < name.length; i++) {
            if(month.compareToIgnoreCase(name[i]) == 0) return true;
        }
        return false;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String month = "";
        int year;
        do{
            System.out.print("Mời bạn nhập tháng: ");
            month = sc.nextLine();
            month = month.trim();
            if(!thangHopLe(month)) System.out.println("Định dạng tháng không hợp lệ!");
        } while (!thangHopLe(month));
        do{
            System.out.print("Năm: ");
            year = sc.nextInt();
            if(year <=0 ) System.out.println("Nhập lại năm (số nguyên dương)");
        } while (year <= 0);
        month = month.toLowerCase();
        System.out.print("\nKết quả: Tháng "+ month + " năm " + year + " có ");
        switch (month){
            case "january":
            case "jan.":
            case "jan":
            case "1":
            case "march":
            case "mar.":
            case "mar":
            case "3":
            case "may":
            case "5":
            case "july":
            case "jul":
            case "7":
            case "august":
            case "aug.":
            case "aug":
            case "8":
            case "october":
            case "oct.":
            case "oct":
            case "10":
            case "december":
            case "dec.":
            case "dec":
            case "12":
                System.out.print("31");
                break;
            case "april":
            case "apr.":
            case "apr":
            case "4":
            case "june":
            case "jun":
            case "6":
            case "september":
            case "sept.":
            case "sep":
            case "9":
            case "november":
            case "nov.":
            case "nov":
            case "11":
                System.out.print("30");
                break;
            case "february":
            case "feb.":
            case "feb":
            case "2":
                if((year % 400 == 0) || (year % 100 != 0 && year % 4 == 0)){
                    System.out.print("29");
                } else System.out.print("28");
                break;
        }
        System.out.println(" ngày\n");
    }
}
