/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab02;

import java.util.Scanner;

/**
 *
 * @author ADMIN
 */
public class InputFromKeyboard {
    public static void main(String[] args) {
        // Khoi tao doi tuong scanner
        Scanner sc = new Scanner(System.in);
        
        // In ra thong bao + lech doc du lieu
        System.out.print("Nhap ten cua bạn: ");
        String strName = sc.nextLine();
        
        System.out.println("Nhap tuoi cua ban: ");
        int iAge = sc.nextInt();
        
        System.out.println("Nhap chieu cao: ");
        double dHeight = sc.nextDouble();
        
        System.out.println("Thong tin da nhap: ");
        System.out.println("Ho va ten: "+ strName + ", "
        + iAge + " tuoi, cao " + dHeight +"m");
    }
}
