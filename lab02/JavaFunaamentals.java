/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab02;

/**
 *
 * @author ADMIN
 */
public class JavaFunaamentals {
    public static void main(String[] args) {
        //1. Chu y khi tinh toan voi kieu so thuc
        System.out.println(2.0 - 1.1);
        System.out.println(5.0 - 4.1);
        
        //2/ Lenh printf trong Java
        System.out.printf("%.2f\n", 2.0 - 1.1);
        
        //3. Khai bao hằng trong Java
        final double PI = 3.14159;
        final long MICROS = 24*60*60*1000*1000; // bị tràn
        final long MILIS = 24*60*60*1000;
        System.out.println(MICROS/MILIS);
        
        //4.Lưu ý font chữ
        System.out.println(12345 + 4321l);
        
        //5. Lưu ý trong chuyển đổi kiểu
        System.out.println((int) (char) (byte) -1);
        
        //6. Char và String trong phép cộng
        System.out.println("H" + "a");
        System.out.println('H' + 'a');
        
        System.out.println(1 + 2 + "3");
        System.out.println("1" + 2 + 3);
        
        //7. Ký tự kiểu char trong java la unicode
        char a = '\u0061';
        System.out.println(a);
        
        ch\u0061r c = 'b';
        System.out.println(c);
    }
}
