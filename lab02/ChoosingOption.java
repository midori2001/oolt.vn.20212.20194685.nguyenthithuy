/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab02;
import javax.swing.JOptionPane;
/**
 *
 * @author ADMIN
 */
public class ChoosingOption {
    public static void main(String[] args) {
        String x = "";
        int option = JOptionPane.showConfirmDialog(null, "リンゴが好き？");
        JOptionPane.showMessageDialog(null, "You're chosen: " +
               (option == JOptionPane.YES_OPTION ? "YES" : (option == JOptionPane.NO_OPTION ? "NO" : "CANCEL")) );
        System.exit(0);
    }
}
