/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab02;

import static java.util.Arrays.sort;
import java.util.Scanner;

/**
 *
 * @author ADMIN
 */
public class HW6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        do {
            System.out.print("Nhap do dai cua mang: ");
            n = sc.nextInt();
            if (n <=0) System.out.println("Nhap so nguyen duong!!");
        } while (n <= 0);
        float arr[] = new float [n];
        float sum = 0;
        System.out.println("Nhap cac phan tu cho mang: ");
        for (int i = 0; i < n; i++) {
            System.out.printf("Arr[%d]: ", i);
            arr[i] = sc.nextFloat();
        }
        sort(arr);
        System.out.println("\nMang sau khi sap xep: ");
        for (int i = 0; i < n; i++) {
            sum += arr[i];
            System.out.print(arr[i] + "\t");
        }
        System.out.println("\nTong cac phan tu cua mang: " + sum);
        System.out.println("Gia trị trung bình cac phan tu mang: " + (sum/n));
    }
}
