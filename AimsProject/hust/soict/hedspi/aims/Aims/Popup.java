package hust.soict.hedspi.aims.Aims;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Track;
import hust.soict.hedspi.aims.order.Order;
import javafx.animation.PauseTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Popup implements Initializable {
    public Label text;
    public TextField line1;
    public ComboBox<String> listItem;
    public TextField line2;
    public TextField line3;
    public Button themButton;
    public ScrollPane scroll;
    public Label label;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        scroll.setViewOrder(99);
        scroll.setVisible(false);
        ObservableList<String> list = FXCollections.observableArrayList("Book", "CompactDisc", "DigitalVideoDisc");
        listItem.setItems(list);
        listItem.getSelectionModel().select(0);
        listItem.valueProperty().addListener(actionEvent -> {
            switch (listItem.getSelectionModel().getSelectedItem()) {
                case "Book" -> {
                    line1.setPromptText("Nhập title Book");
                    line2.setPromptText("Nhập Category");
                    line3.setPromptText("Nhập Author");
                }
                case "CompactDisc" -> {
                    line1.setPromptText("Nhập artist CompactDisc");
                    line2.setPromptText("Nhập title Tracks");
                    line3.setPromptText("Nhập length Tracks");
                }
                case "DigitalVideoDisc" -> {
                    line1.setPromptText("Nhập title DigitalVideoDisc");
                    line2.setPromptText("Nhập Category");
                    line3.setPromptText("Nhập Director");
                }
                default -> {
                }
            }
        });
    }

    public void exit(MouseEvent mouseEvent) {
        Stage window = (Stage) text.getScene().getWindow();
        window.close();

    }

    public void add(Order order) {
        themButton.setOnMouseClicked(mouseEvent -> {
            switch (listItem.getSelectionModel().getSelectedItem()) {
                case "Book" -> {
                    List<String> authors = new ArrayList<>();
                    authors.add(line3.getText().trim());
                    order.addMedia(new Book(line1.getText().trim(), line2.getText().trim(), authors));
                }
                case "CompactDisc" -> {
                    Track track = new Track(line2.getText().trim(), Integer.parseInt(line3.getText().trim()));
                    ArrayList<Track> tracks = new ArrayList<>();
                    tracks.add(track);
                    order.addMedia(new CompactDisc(line1.getText().trim(), tracks));
                }
                case "DigitalVideoDisc" ->
                        order.addMedia(new DigitalVideoDisc(line1.getText().trim(), line2.getText().trim(), line3.getText().trim()));
                default -> {
                }
            }
            line1.setText("");
            line2.setText("");
            line3.setText("");
            alert("Thêm thành công");
        });
    }

    private void alert(String message) {
        Stage alert = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("alert.fxml"));
        Parent pr;
        try {
            pr = loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Alert controller = loader.getController();
        controller.setTextAlert(message);
        Scene sc1 = new Scene(pr);
        alert.setScene(sc1);
        sc1.setFill(Color.TRANSPARENT);
        alert.initStyle(StageStyle.TRANSPARENT);
        alert.setX(text.getScene().getWindow().getX() + 25);
        alert.setY(text.getScene().getWindow().getY() + 70);
        alert.setAlwaysOnTop(true);
        alert.show();
        PauseTransition delay = new PauseTransition(Duration.seconds(2));
        delay.setOnFinished(event -> alert.close());
        delay.play();
    }

    void show(Order order) {
        scroll.setViewOrder(0);
        scroll.setVisible(true);
        label.setText(order.getAll());
    }

    public void remove(Order order) {
        listItem.setVisible(false);
        line2.setVisible(false);
        line3.setVisible(false);
        themButton.setText("Xóa");
        line1.setPromptText("id");
        themButton.setOnMouseClicked(mouseEvent -> {
            order.removeMedia(Integer.parseInt(line1.getText().trim()));
            alert("Xóa thành công");
        });
    }
}
