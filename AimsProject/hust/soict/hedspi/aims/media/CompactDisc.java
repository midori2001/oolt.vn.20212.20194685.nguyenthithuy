/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hust.soict.hedspi.aims.media;

import hust.soict.hedspi.aims.Aims.PlayerException;

import java.util.ArrayList;

/**
 * @author ADMIN
 */
public class CompactDisc extends Disc implements Playable, Comparable<CompactDisc> {
    private String artist;
    private ArrayList<Track> tracks = new ArrayList<>();

    public String getArtist() {
        return artist;
    }

    public CompactDisc(String artist, ArrayList<Track> tracks) {
        this.artist = artist;
        this.tracks = tracks;
    }

    public int getLength(ArrayList<Track> tracks) {
        int sumlength = 0;
        for (int i = 0; i < tracks.size(); i++) {
            sumlength += tracks.get(i).getLength();
        }
        super.length = sumlength;
        return super.length;
    }

    public void addTrack(Track track) {
        if (tracks.contains(track)) {
            System.out.println("Already exist");
        } else {
            tracks.add(track);
        }
    }

    public void removeTrack(Track track) {
        if (!tracks.contains(track)) {
            System.out.println("Not exist");
        } else {
            tracks.remove(track);
        }
    }

    @Override
    public void play() throws PlayerException {
        if (this.getLength() <= 0) {
            System.out.println("ERROR: CD length is 0");
            throw (new PlayerException());
        }
        System.out.println("Playing CD: " + this.getTitle());
        System.out.println("CD length: " + this.getLength());

        java.util.Iterator iter = tracks.iterator();
        Track nextTrack;

        while (iter.hasNext()) {
            nextTrack = (Track) iter.next();
            try {
                nextTrack.play();
            } catch (PlayerException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int compareTo(CompactDisc o) {
        return this.id - o.id;
    }

    @Override
    public String toString() {
        return "CompactDisc{" +
                "artist: '" + artist + '\'' +
                ", title track: " + tracks.get(0).getTitle() +
                ", length track: " + tracks.get(0).getLength() +
                '}';
    }
}
